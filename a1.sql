-- Find partial matches with letter d in name
SELECT * FROM artists WHERE name LIKE "%d%";

-- Find songs less than 230 length
SELECT * FROM songs WHERE length < 230;

-- Joining of two tables
SELECT album_title, song_name, length FROM albums
	JOIN songs ON albums.id = songs.album_id; 

-- Joining of two tables with letter 'a' in album_title
SELECT * FROM artists
	JOIN albums ON artists.id = albums.artist_id
	WHERE album_title LIKE "%a%"; 

-- sorting of records of albums table; first 4 records only.
SELECT * FROM albums ORDER BY album_title DESC
LIMIT 4;

-- Joining of two tables and sort Z-A by album_title
SELECT * FROM albums
	JOIN songs ON albums.id = songs.album_id
	ORDER BY album_title DESC;